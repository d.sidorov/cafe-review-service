<?php

namespace Misc;


use Ramsey\Uuid\Uuid;

class RamseyUUIDGeneratorAdapter implements \UUIDGeneratorInterface
{
    /**
     * This meth must return random generated UUID string value
     * @return string
     * @throws \Exception
     */
    public function generate(): string
    {
        return Uuid::uuid4()->toString();
    }
}