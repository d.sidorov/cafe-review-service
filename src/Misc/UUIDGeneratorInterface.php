<?php

interface UUIDGeneratorInterface
{
    /**
     * This meth must return random generated UUID string value
     * @return string
     */
    public function generate(): string;
}