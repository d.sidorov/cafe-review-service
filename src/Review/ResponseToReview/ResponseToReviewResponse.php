<?php

namespace CafeReview\Review\ResponseToReview;


class ResponseToReviewResponse
{
    /**
     * @var string
     */
    public $text;

    /**
     * ResponseToReviewResponse constructor.
     * @param string $text
     */
    public function __construct(string $text)
    {
        $this->text = $text;
    }
}