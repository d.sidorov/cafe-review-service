<?php

namespace CafeReview\Review\ResponseToReview;


interface ResponseToReviewInterface
{
    public function execute(ResponseToReviewRequest $req): ResponseToReviewResponse;
}