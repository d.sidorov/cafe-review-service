<?php

namespace CafeReview\Review\ResponseToReview;


use CafeReview\Review\ReviewRepositoryInterface;
use CafeReview\Review\ReviewResponse;

class ResponseToReviewUseCase implements ResponseToReviewInterface
{
    /**
     * @var ReviewRepositoryInterface
     */
    private $reviewRepository;

    public function __construct(ReviewRepositoryInterface $reviewRepository)
    {
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * @param ResponseToReviewRequest $req
     * @return ResponseToReviewResponse
     * @throws ReviewNotFoundException
     */
    public function execute(ResponseToReviewRequest $req): ResponseToReviewResponse
    {
        $review = $this->reviewRepository->get($req->reviewId, $req->cafeId);

        $response = new ReviewResponse($req->text);
        $review->setResponse($response);

        $this->reviewRepository->update($review);

        return new ResponseToReviewResponse($review->getResponse());
    }
}