<?php

namespace CafeReview\Review\ResponseToReview;


class ResponseToReviewRequest
{
    /**
     * @var string
     */
    public $reviewId;
    /**
     * @var string
     */
    public $cafeId;
    /**
     * @var string
     */
    public $text;

    /**
     * ResponseToReviewRequest constructor.
     * @param string $reviewId
     * @param string $cafeId
     * @param string $text
     */
    public function __construct(string $reviewId, string $cafeId, string $text)
    {
        $this->reviewId = $reviewId;
        $this->cafeId = $cafeId;
        $this->text = $text;
    }
}