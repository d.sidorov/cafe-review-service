<?php

namespace CafeReview\Review;


use CafeReview\Review\ResponseToReview\ReviewNotFoundException;

interface ReviewRepositoryInterface
{
    /**
     * @param string $cafeId
     * @return Review[]
     */
    public function getListByCafeId(string $cafeId): array;

    public function add(Review $review): void;

    /**
     * @param string $reviewId
     * @param string $cafeId
     * @return Review
     * @throws ReviewNotFoundException
     */
    public function get(string $reviewId, string $cafeId): Review;

    public function update(Review $review): void;
}