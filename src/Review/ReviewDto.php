<?php

namespace CafeReview\Review;


class ReviewDto
{
    /**
     * @var string
     */
    public $reviewId;

    /**
     * @var string
     */
    public $authorId;

    /**
     * @var string
     */
    public $cafeId;

    /**
     * @var string
     */
    public $text;

    /**
     * @var int
     */
    public $stars;

    public function __construct(string $reviewId, string $authorId, string $cafeId, string $text, int $stars)
    {
        $this->reviewId = $reviewId;
        $this->authorId = $authorId;
        $this->cafeId = $cafeId;
        $this->text = $text;
        $this->stars = $stars;
    }
}