<?php

namespace CafeReview\Review;


class Review
{
    const MAX_STARS = 5;
    const MIN_STARS = 1;
    /**
     * @var string
     */
    private $id;

    /**
     * @var string
     */
    private $authorId;

    /**
     * @var string
     */
    private $cafeId;

    /**
     * @var string
     */
    private $text;

    /**
     * @var int
     */
    private $stars;

    /**
     * @var ReviewResponse|null
     */
    private $response;

    /**
     * Review constructor.
     * @param string $id
     * @param string $authorId
     * @param string $cafeId
     * @param string $text
     * @param int $stars
     * @throws StarsOutOfRangeException
     */
    public function __construct(string $id, string $authorId, string $cafeId, string $text, int $stars)
    {
        $this->id = $id;
        $this->authorId = $authorId;
        $this->cafeId = $cafeId;
        $this->text = $text;

        if ($stars < self::MIN_STARS || $stars > self::MAX_STARS) {
            throw new StarsOutOfRangeException("stars must be between 0 and 5. Given: ${stars}");
        }

        $this->stars = $stars;
    }

    /**
     * @return string
     */
    public function getId(): string
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getAuthorId(): string
    {
        return $this->authorId;
    }

    /**
     * @return string
     */
    public function getCafeId(): string
    {
        return $this->cafeId;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }

    /**
     * @return int
     */
    public function getStars(): int
    {
        return $this->stars;
    }

    /**
     * @return ReviewResponse|null
     */
    public function getResponse(): ?ReviewResponse
    {
        return $this->response;
    }

    /**
     * @param ReviewResponse|null $response
     */
    public function setResponse(?ReviewResponse $response): void
    {
        $this->response = $response;
    }
}