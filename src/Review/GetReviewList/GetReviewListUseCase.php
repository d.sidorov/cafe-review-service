<?php

namespace CafeReview\Review\GetReviewList;


use CafeReview\Review\Review;
use CafeReview\Review\ReviewDto;
use CafeReview\Review\ReviewRepositoryInterface;

class GetReviewListUseCase implements GetReviewListUseCaseInterface
{
    /**
     * @var ReviewRepositoryInterface
     */
    private $reviewRepository;

    public function __construct(ReviewRepositoryInterface $reviewRepository)
    {
        $this->reviewRepository = $reviewRepository;
    }

    public function execute(GetReviewListRequest $request): GetReviewListResponse
    {
        $reviews = $this->reviewRepository->getListByCafeId($request->cafeId);
        $dtos = array_map(function (Review $review) {
            return new ReviewDto(
                $review->getId(),
                $review->getAuthorId(),
                $review->getCafeId(),
                $review->getText(),
                $review->getStars()
            );
        }, $reviews);

        return new GetReviewListResponse($dtos);
    }
}