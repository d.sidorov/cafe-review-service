<?php

namespace CafeReview\Review\GetReviewList;


use CafeReview\Review\ReviewDto;

class GetReviewListResponse
{
    /**
     * @var ReviewDto[]
     */
    public $reviewDtos;

    /**
     * @param ReviewDto[] $reviewDtos
     */
    public function __construct(array $reviewDtos)
    {
        $this->reviewDtos = $reviewDtos;
    }
}