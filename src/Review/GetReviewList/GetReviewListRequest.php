<?php

namespace CafeReview\Review\GetReviewList;


class GetReviewListRequest
{
    /**
     * @var string
     */
    public $cafeId;

    public function __construct(string $cafeId)
    {
        $this->cafeId = $cafeId;
    }
}