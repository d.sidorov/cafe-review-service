<?php

namespace CafeReview\Review\GetReviewList;


interface GetReviewListUseCaseInterface
{
    public function execute(GetReviewListRequest $request): GetReviewListResponse;
}