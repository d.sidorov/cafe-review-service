<?php

namespace CafeReview\Review\PlaceReview;


use CafeReview\Review\Review;
use CafeReview\Review\ReviewDto;
use CafeReview\Review\ReviewRepositoryInterface;

class PlaceReviewUseCase implements PlaceReviewUseCaseInterface
{
    /**
     * @var \UUIDGeneratorInterface
     */
    private $uuidGenerator;
    /**
     * @var ReviewRepositoryInterface
     */
    private $reviewRepository;

    public function __construct(\UUIDGeneratorInterface $uuidGenerator, ReviewRepositoryInterface $reviewRepository)
    {
        $this->uuidGenerator = $uuidGenerator;
        $this->reviewRepository = $reviewRepository;
    }

    /**
     * @param PlaceReviewRequest $req
     * @return PlaceReviewResponse
     * @throws \CafeReview\Review\StarsOutOfRangeException
     */
    public function execute(PlaceReviewRequest $req): PlaceReviewResponse
    {
        $uuid = $this->uuidGenerator->generate();
        $review = new Review(
            $uuid,
            $req->authorId,
            $req->cafeId,
            $req->text,
            $req->stars
        );

        $this->reviewRepository->add($review);

        $dto = new ReviewDto(
            $review->getId(),
            $review->getAuthorId(),
            $review->getCafeId(),
            $review->getText(),
            $review->getStars()
        );
        return new PlaceReviewResponse($dto);
    }
}