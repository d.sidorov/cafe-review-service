<?php

namespace CafeReview\Review\PlaceReview;


interface PlaceReviewUseCaseInterface
{
    public function execute(PlaceReviewRequest $req): PlaceReviewResponse;
}