<?php

namespace CafeReview\Review\PlaceReview;


use CafeReview\Review\ReviewDto;

class PlaceReviewResponse
{
    /**
     * @var ReviewDto
     */
    public $reviewDto;

    /**
     * PlaceReviewResponse constructor.
     * @param ReviewDto $reviewDto
     */
    public function __construct(ReviewDto $reviewDto)
    {
        $this->reviewDto = $reviewDto;
    }
}