<?php

namespace CafeReview\Review\PlaceReview;


class PlaceReviewRequest
{
    /**
     * @var string
     */
    public $authorId;

    /**
     * @var string
     */
    public $cafeId;

    /**
     * @var string
     */
    public $text;

    /**
     * @var int
     */
    public $stars;

    public function __construct(string $authorId, string $cafeId, string $text, int $stars)
    {
        $this->authorId = $authorId;
        $this->cafeId = $cafeId;
        $this->text = $text;
        $this->stars = $stars;
    }
}