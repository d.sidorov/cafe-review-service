<?php

namespace CafeReview\Cafe\CreateCafe;


interface CreateCafeUseCaseInterface
{
    public function execute(CreateCafeRequest $req): CreateCafeResponse;
}