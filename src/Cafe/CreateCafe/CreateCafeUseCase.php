<?php

namespace CafeReview\Cafe\CreateCafe;


use CafeReview\Cafe\Cafe;
use CafeReview\Cafe\CafeDTO;
use CafeReview\Cafe\CafeRepositoryInterface;

class CreateCafeUseCase implements CreateCafeUseCaseInterface
{
    /**
     * @var \UUIDGeneratorInterface
     */
    private $UUIDGenerator;
    /**
     * @var CafeRepositoryInterface
     */
    private $cafeRepository;

    public function __construct(\UUIDGeneratorInterface $UUIDGenerator, CafeRepositoryInterface $cafeRepository)
    {
        $this->UUIDGenerator = $UUIDGenerator;
        $this->cafeRepository = $cafeRepository;
    }

    public function execute(CreateCafeRequest $req): CreateCafeResponse
    {
        $uuid = $this->UUIDGenerator->generate();
        $newCafe = new Cafe($uuid, $req->name, $req->address);
        $this->cafeRepository->add($newCafe);

        $cafeDto = new CafeDTO($newCafe->getId(), $newCafe->getName(), $newCafe->getAddress());
        return new CreateCafeResponse($cafeDto);
    }
}