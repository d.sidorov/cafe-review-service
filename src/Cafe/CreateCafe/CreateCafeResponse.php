<?php

namespace CafeReview\Cafe\CreateCafe;


use CafeReview\Cafe\CafeDTO;

class CreateCafeResponse
{
    /**
     * @var CafeDTO
     */
    public $cafeDto;

    /**
     * CreateCafeResponse constructor.
     * @param CafeDTO $cafeDto
     */
    public function __construct(CafeDTO $cafeDto)
    {
        $this->cafeDto = $cafeDto;
    }
}