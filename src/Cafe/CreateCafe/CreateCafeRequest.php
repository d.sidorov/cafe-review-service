<?php

namespace CafeReview\Cafe\CreateCafe;


class CreateCafeRequest
{
    /**
     * @var string
     */
    public $name;
    /**
     * @var string
     */
    public $address;

    /**
     * CreateCafeRequest constructor.
     * @param string $name
     * @param string $address
     */
    public function __construct(string $name, string $address)
    {
        $this->name = $name;
        $this->address = $address;
    }
}