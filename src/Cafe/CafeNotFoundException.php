<?php

namespace CafeReview\Cafe;


class CafeNotFoundException extends \Exception
{
    public function __construct(string $cafeId)
    {
        parent::__construct("Cafe with given cafeId (${cafeId}) not found", 0, null);
    }
}