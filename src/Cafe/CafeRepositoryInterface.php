<?php

namespace CafeReview\Cafe;


interface CafeRepositoryInterface
{
    /**
     * @return Cafe[]
     */
    public function getAll(): array;

    /**
     * @param string $cafeId
     * @return Cafe
     * @throws CafeNotFoundException
     */
    public function get(string $cafeId): Cafe;

    /**
     * @param Cafe $newCafe
     * @throws CafeAlreadyExistException
     */
    public function add(Cafe $newCafe): void;
}