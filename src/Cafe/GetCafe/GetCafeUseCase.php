<?php

namespace CafeReview\Cafe\GetCafe;


use CafeReview\Cafe\CafeDTO;
use CafeReview\Cafe\CafeRepositoryInterface;

class GetCafeUseCase implements GetCafeUseCaseInterface
{
    /**
     * @var CafeRepositoryInterface
     */
    private $cafeRepository;

    public function __construct(CafeRepositoryInterface $cafeRepository)
    {
        $this->cafeRepository = $cafeRepository;
    }

    /**
     * @param GetCafeRequest $req
     * @return GetCafeResponse
     * @throws \CafeReview\Cafe\CafeNotFoundException
     */
    public function execute(GetCafeRequest $req): GetCafeResponse
    {
        $cafe = $this->cafeRepository->get($req->getCafeId());
        $cafeDto = new CafeDTO($cafe->getId(), $cafe->getName(), $cafe->getAddress());
        return new GetCafeResponse($cafeDto);
    }
}