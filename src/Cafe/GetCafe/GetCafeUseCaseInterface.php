<?php

namespace CafeReview\Cafe\GetCafe;


interface GetCafeUseCaseInterface
{
    public function execute(GetCafeRequest $req): GetCafeResponse;
}