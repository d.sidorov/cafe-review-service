<?php

namespace CafeReview\Cafe\GetCafe;


use CafeReview\Cafe\CafeDTO;

class GetCafeResponse
{
    /**
     * @var CafeDTO
     */
    public $cafeDto;

    public function __construct(CafeDTO $cafeDto)
    {
        $this->cafeDto = $cafeDto;
    }
}