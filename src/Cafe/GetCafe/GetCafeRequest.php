<?php

namespace CafeReview\Cafe\GetCafe;


class GetCafeRequest
{
    /**
     * @var string
     */
    private $cafeId;

    /**
     * GetCafeRequest constructor.
     * @param string $cafeId
     */
    public function __construct(string $cafeId)
    {
        $this->cafeId = $cafeId;
    }

    /**
     * @return string
     */
    public function getCafeId(): string
    {
        return $this->cafeId;
    }
}