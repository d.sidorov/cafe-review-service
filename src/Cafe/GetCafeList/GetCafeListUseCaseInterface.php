<?php

namespace CafeReview\Cafe\GetCafeList;


interface GetCafeListUseCaseInterface
{
    public function execute(): GetCafeListResponse;
}