<?php

namespace CafeReview\Cafe\GetCafeList;

use CafeReview\Cafe\Cafe;
use CafeReview\Cafe\CafeDTO;
use CafeReview\Cafe\CafeRepositoryInterface;

class GetCafeListUseCase implements GetCafeListUseCaseInterface
{
    /**
     * @var CafeRepositoryInterface
     */
    private $cafeRepository;

    public function __construct(CafeRepositoryInterface $cafeRepository)
    {
        $this->cafeRepository = $cafeRepository;
    }

    public function execute(): GetCafeListResponse
    {
        $cafeList = $this->cafeRepository->getAll();
        $cafeDtoList = array_map(function (Cafe $cafe) {
            return new CafeDTO($cafe->getId(), $cafe->getName(), $cafe->getAddress());
        }, $cafeList);

        return new GetCafeListResponse($cafeDtoList);
    }
}