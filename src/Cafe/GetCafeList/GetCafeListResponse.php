<?php

namespace CafeReview\Cafe\GetCafeList;

class GetCafeListResponse
{
    /**
     * @var array
     */
    public $cafeList;

    public function __construct(array $cafeList)
    {
        $this->cafeList = $cafeList;
    }
}