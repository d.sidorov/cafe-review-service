<?php

use CafeReview\Review\GetReviewList\GetReviewListRequest;
use CafeReview\Review\GetReviewList\GetReviewListResponse;
use CafeReview\Review\GetReviewList\GetReviewListUseCase;
use CafeReview\Review\Review;
use CafeReview\Review\ReviewDto;
use CafeReview\Review\ReviewRepositoryInterface;
use PHPUnit\Framework\TestCase;

class GetReviewListUseCaseTest extends TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    public function testExecute()
    {
        $repo = Mockery::mock(ReviewRepositoryInterface::class);
        $uc = new GetReviewListUseCase($repo);

        $reviews = [
            new Review(
                "first",
                "Alice",
                "Foo",
                "Nice place",
                4
            ),
        ];
        $repo->shouldReceive('getListByCafeId')
            ->once()
            ->andReturn($reviews);

        $req = new GetReviewListRequest("Foo");
        $actual = $uc->execute($req);

        $reviewDtos = [
            new ReviewDto(
                "first",
                "Alice",
                "Foo",
                "Nice place",
                4
            ),
        ];
        $expected = new GetReviewListResponse($reviewDtos);
        $this->assertEquals($expected, $actual);
    }
}