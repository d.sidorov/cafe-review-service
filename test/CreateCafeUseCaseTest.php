<?php

use CafeReview\Cafe\CafeDTO;
use CafeReview\Cafe\CafeRepositoryInterface;
use CafeReview\Cafe\CreateCafe\CreateCafeRequest;
use CafeReview\Cafe\CreateCafe\CreateCafeResponse;
use CafeReview\Cafe\CreateCafe\CreateCafeUseCase;
use PHPUnit\Framework\TestCase;

class CreateCafeUseCaseTest extends TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    public function testExecute()
    {
        $uuidGenerator = Mockery::mock(UUIDGeneratorInterface::class);
        $repo = Mockery::mock(CafeRepositoryInterface::class);
        $uc = new CreateCafeUseCase($uuidGenerator, $repo);

        $req = new CreateCafeRequest("Foo", "Bar st.");
        $uuidGenerator->shouldReceive('generate')->andReturn("first");

        $repo->shouldReceive('add')->once();

        $res = $uc->execute($req);

        $cafeDto = new CafeDTO("first", "Foo", "Bar st.");
        $expected = new CreateCafeResponse($cafeDto);
        $this->assertEquals($expected, $res);
    }
}