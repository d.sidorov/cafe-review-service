<?php

use CafeReview\Cafe\Cafe;
use CafeReview\Cafe\CafeDTO;
use CafeReview\Cafe\CafeRepositoryInterface;
use CafeReview\Cafe\GetCafeList\GetCafeListResponse;
use CafeReview\Cafe\GetCafeList\GetCafeListUseCase;
use PHPUnit\Framework\TestCase;

class GetCafeListUseCaseTest extends TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    public function testExecute()
    {
        $cafeRepo = Mockery::mock(CafeRepositoryInterface::class);
        $cafeService = new GetCafeListUseCase($cafeRepo);

        $emptyCafeList = [];
        $cafeRepo->shouldReceive('getAll')->once()->andReturn($emptyCafeList);

        $expected = new GetCafeListResponse([]);
        $actual = $cafeService->execute();
        $this->assertEquals($expected, $actual);

        $cafeList = [
            new Cafe("first", "Foo", "Bar st."),
        ];
        $cafeRepo->shouldReceive('getAll')->once()->andReturn($cafeList);

        $cafeDtoList = [
            new CafeDTO("first", "Foo", "Bar st."),
        ];
        $expected = new GetCafeListResponse($cafeDtoList);
        $actual = $cafeService->execute();
        $this->assertEquals($expected, $actual);
    }
}
