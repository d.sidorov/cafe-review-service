<?php

use CafeReview\Cafe\Cafe;
use CafeReview\Cafe\CafeDTO;
use CafeReview\Cafe\CafeNotFoundException;
use CafeReview\Cafe\CafeRepositoryInterface;
use CafeReview\Cafe\GetCafe\GetCafeRequest;
use CafeReview\Cafe\GetCafe\GetCafeResponse;
use CafeReview\Cafe\GetCafe\GetCafeUseCase;
use PHPUnit\Framework\TestCase;

class GetCafeUseCaseTest extends TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    public function testExecute()
    {
        $repo = Mockery::mock(CafeRepositoryInterface::class);
        $uc = new GetCafeUseCase($repo);

        $cafe = new Cafe("first", "Foo", "Bar st.");
        $repo->shouldReceive('get')
            ->with("Foo")
            ->once()
            ->andReturn($cafe);

        $req = new GetCafeRequest("Foo");
        $res = $uc->execute($req);

        $cafeDto = new CafeDTO("first", "Foo", "Bar st.");
        $expected = new GetCafeResponse($cafeDto);
        $this->assertEquals($expected, $res);
    }

    /**
     * @expectedException CafeReview\Cafe\CafeNotFoundException
     */
    public function testExecuteThrowCafeNotFoundException()
    {
        $repo = Mockery::mock(CafeRepositoryInterface::class);
        $uc = new GetCafeUseCase($repo);

        $repo->shouldReceive('get')->andThrow(new CafeNotFoundException("Foo"));

        $req = new GetCafeRequest("Foo");
        $uc->execute($req);
    }
}
