<?php

use CafeReview\Review\PlaceReview\PlaceReviewRequest;
use CafeReview\Review\PlaceReview\PlaceReviewResponse;
use CafeReview\Review\PlaceReview\PlaceReviewUseCase;
use CafeReview\Review\ReviewDto;
use CafeReview\Review\ReviewRepositoryInterface;
use PHPUnit\Framework\TestCase;

class PlaceReviewTest extends TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    public function testExecute()
    {
        $uuidGenerator = Mockery::mock(UUIDGeneratorInterface::class);
        $reviewRepo = Mockery::mock(ReviewRepositoryInterface::class);
        $uc = new PlaceReviewUseCase($uuidGenerator, $reviewRepo);

        $uuidGenerator->shouldReceive('generate')->andReturn("first");
        $reviewRepo->shouldReceive('add')->once();

        $req = new PlaceReviewRequest("Alice", "Foo", "It`s awesome place!", 5);
        $res = $uc->execute($req);

        $reviewDto = new ReviewDto("first", "Alice", "Foo", "It`s awesome place!", 5);
        $expected = new PlaceReviewResponse($reviewDto);
        $this->assertEquals($expected, $res);
    }
}