<?php

use CafeReview\Review\ResponseToReview\ResponseToReviewRequest;
use CafeReview\Review\ResponseToReview\ResponseToReviewResponse;
use CafeReview\Review\ResponseToReview\ResponseToReviewUseCase;
use CafeReview\Review\Review;
use CafeReview\Review\ReviewRepositoryInterface;
use CafeReview\Review\ReviewResponse;
use PHPUnit\Framework\TestCase;

class ResponseToReviewUseCaseTest extends TestCase
{
    public function tearDown()
    {
        Mockery::close();
    }

    public function testExecute()
    {
        $reviewRepo = Mockery::mock(ReviewRepositoryInterface::class);
        $uc = new ResponseToReviewUseCase($reviewRepo);

        $badReview = new Review(
            "BadReviewId",
            "Alice",
            "Foo Cafe",
            "It`s bad place",
            2
        );
        $reviewRepo->shouldReceive('get')
            ->with("BadReviewId", "Foo Cafe")
            ->once()
            ->andReturn($badReview);

        $text = "We will take action";
        $response = new ReviewResponse($text);
        $badReview->setResponse($response);

        $reviewRepo->shouldReceive('update')
            ->with($badReview)
            ->once();

        $req = new ResponseToReviewRequest("BadReviewId", "Foo Cafe", $text);
        $expected = new ResponseToReviewResponse($text);
        $actual = $uc->execute($req);
        $this->assertEquals($expected, $actual);
    }
}