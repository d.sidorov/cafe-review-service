<?php

use CafeReview\Review\Review;
use PHPUnit\Framework\TestCase;

class ReviewTest extends TestCase
{
    /**
     * @expectedException \CafeReview\Review\StarsOutOfRangeException
     */
    public function testConstructThrowStarsOutOfRangeException()
    {
        new Review(
            "first",
            "Alice",
            "Foo",
            "It`s such awesome place!",
            Review::MAX_STARS + 1
        );
    }

    /**
     * @expectedException \CafeReview\Review\StarsOutOfRangeException
     */
    public function testConstructThrowStarsOutOfRangeException2()
    {
        new Review(
            "first",
            "Alice",
            "Foo",
            "It`s so awful place ever!",
            Review::MIN_STARS - 1
        );
    }
}